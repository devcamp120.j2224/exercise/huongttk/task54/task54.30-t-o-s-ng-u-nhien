package com.devcamp.s50.task5430.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		// khai báo đối tượng class CRandomNumber
		CRandomNumber randomNumber = new CRandomNumber();
		// In ra random kiểu dữ liệu double
		System.out.println("Kieu du lieu double: " + randomNumber.randomDoubleNumber());
		// In ra random kiểu dữ liệu int
		System.out.println("Kieu du lieu int: " + randomNumber.randomIntNumber());
	}

}
