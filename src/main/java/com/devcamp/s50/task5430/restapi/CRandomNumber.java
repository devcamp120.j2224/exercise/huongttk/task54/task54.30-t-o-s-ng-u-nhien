package com.devcamp.s50.task5430.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CRandomNumber {
    @GetMapping("/devcamp/random-double")

    public String randomDoubleNumber() {
        double randomDoubleNumber = 0;
        for (int i = 1; i < 100; i++){
            randomDoubleNumber = Math.random()*100;
        }
        return ("Random value in double from 1 to 100: " + randomDoubleNumber);
    }

    @GetMapping("/devcamp/random-int")

    public String randomIntNumber() {
        int randomIntNumber = 1 + (int)(Math.random()*10);
      
     
        return ("Random value in double int 1 to 10: " + randomIntNumber);
    }

}
